import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { ProduitComponent } from '../produit/produit.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogComponent } from 'src/app/dialog/dialog.component';
import { DialogConfig } from '@angular/cdk/dialog';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DialogUpdateComponent } from '../dialog-update/dialog-update.component';
@Component({
  selector: 'app-back',
  templateUrl: './back.component.html',
  styleUrls: ['./back.component.css']
})
export class BackComponent implements OnInit {
 
  usersForm!: FormGroup;
  produit: any;

dialogConfig = new MatDialogConfig();

  constructor(
    private serviceService: ServiceService, 
    private dialogBack: MatDialog,
    private formBuilder: FormBuilder) {
this.usersForm = this.formBuilder.group({
  users: this.formBuilder.array([])
})


   }

   initForm() {
    this.usersForm = this.formBuilder.group({
      nom_produit: ['', Validators.required],
      categorie_produit: ['', Validators.required],
      description: ['', Validators.required],
      photo_produit: ['', Validators.required],
      prix_produit: ['', Validators.required],
      
    
    });
  }
 


  ngOnInit(): void {
    this.getAllProduits();
    this.initForm()
  }

  getAllProduits(): void {
    this.serviceService.GetProduits().subscribe((data: any) => {
        this.produit = data;
      
        console.log(this.produit);
      },
      )
  }
 

addOne(): void {
  this.serviceService.AddOneProduit(this.usersForm.value).subscribe({
    next: (data: any) => {
      
      console.log(data);
       this.initForm();
       this.getAllProduits()
    }
  })
}



delete(id_produit: any) {
  this.serviceService.DeleteOneProduit(id_produit).subscribe({next: (id_produit: any) => {
    console.log(id_produit)
      alert("Le produit à bien été supprimé"),
      this.getAllProduits()
    }
  })
}


openDialog(data: any){
  console.log(data)
  this.dialogConfig.data = {
    data: data,
  }
  this.dialogConfig.width = "20%"
  this.dialogConfig.height = "75%";
  console.log(this.dialogConfig)
  this.dialogBack.open(DialogUpdateComponent, this.dialogConfig)
}
}


