import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { BackComponent } from './back/back.component';
import { ContactComponent } from './contact/contact.component';
import { ProduitComponent } from './produit/produit.component'
import { RealisationsComponent } from './realisations/realisations.component';

const routes: Routes = [
 { path: '', component: AccueilComponent},
 { path: 'produits', component: ProduitComponent},
 { path: 'contact', component: ContactComponent},
 { path: 'realisations', component: RealisationsComponent},
 { path: 'back', component: BackComponent},
 { path: 'back-delete/:id', component: BackComponent },
 { path: 'back-update/:id', component: BackComponent },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
