import { AnyCatcher } from 'rxjs/internal/AnyCatcher';

export class Produit {
  _id!: Number;
  nom_produit!: String;
  categorie_produit!: String;
  description!: String;
  photo_produit!: String;
  prix_produit!: Number;
}
