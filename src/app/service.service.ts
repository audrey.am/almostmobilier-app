import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Produit } from './produit';
import { Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  REST_API: string ='http://localhost:4000';
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) {

    
   }
   GetProduits(): Observable<Produit[]> {
    return this.httpClient.get<Produit[]>(`${this.REST_API}/produit`)
  }
  GetPlantes(): Observable<any> {
    return this.httpClient.get<any>(`${this.REST_API}/produit/plante`)
  }
  GetTableaux(): Observable<any> {
    return this.httpClient.get<any>(`${this.REST_API}/produit/tableau`)
  }
  GetLuminaires(): Observable<any> {
    return this.httpClient.get<any>(`${this.REST_API}/produit/luminaire`)
  }
  AddOneProduit(data: any): Observable<any> {
    
    return this.httpClient.post<any>(`${this.REST_API}/produit`, data)
  }
  DeleteOneProduit(id_produit: any) {
    return this.httpClient.delete(`${this.REST_API}/produit/${id_produit}`)
  }
  UpdateOneProduit(id_produit: any, data: any) {
    console.log(id_produit)
    return this.httpClient.patch(`${this.REST_API}/produit/${id_produit}`, data)
  }

}
