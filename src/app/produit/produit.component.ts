import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { BackComponent } from '../back/back.component';

import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { DialogConfig } from '@angular/cdk/dialog';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css'],
})
export class ProduitComponent implements OnInit {
  usersForm!: FormGroup;
  

  produit: any;
  plante: any;
  tableau: any;
  luminaire: any;
  dialogConfig = new MatDialogConfig();

  constructor(
    private serviceService: ServiceService, 
    private dialog: MatDialog,
    private formBuilder: FormBuilder) {
this.usersForm = this.formBuilder.group({
  users: this.formBuilder.array([])
}) }
  initForm() {
    this.usersForm = this.formBuilder.group({
      categorie_produit: ['', Validators.required],
      nom_produit: ['', Validators.required],
      description: ['', Validators.required],
      photo_produit: ['', Validators.required],
      prix_produit: ['', Validators.required],
    
    });
  }
  ngOnInit(): void {
    this.getAllPlantes();
    this.getAllTableaux();
    this.getAllLuminaires();
  }
  getAllProduits(): void {
    this.serviceService.GetProduits().subscribe((data: any) => {
        this.produit = data;
        console.log(this.produit);
      },
      )
  }
  getAllPlantes(): void {
    this.serviceService.GetPlantes().subscribe((data: any) => {
        this.plante = data;
        console.log(this.plante);
      },
      )
  }
  
  getAllTableaux(): void {
    this.serviceService.GetTableaux().subscribe((data: any) => {
        this.tableau = data;
        console.log(this.tableau);
      },
      )
  }
  
  getAllLuminaires(): void {
    this.serviceService.GetLuminaires().subscribe((data: any) => {
        this.luminaire = data;
        console.log(this.luminaire);
      },
      )
  }

openDialog(data: any){
  console.log(data)
  this.dialogConfig.data = {
    data: data,
  }
  this.dialogConfig.width = "60%"
  this.dialogConfig.height = "60%";
  console.log(this.dialogConfig)
  this.dialog.open(DialogComponent, this.dialogConfig)
}
}

