import { Component, OnInit } from '@angular/core';
import { Router } from'@angular/router';
import { ServiceService } from "../service.service";

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {

  constructor(private serviceService : ServiceService) { }

  ngOnInit(): void {
  }

}

