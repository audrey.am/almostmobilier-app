import { Component, Inject, OnInit} from '@angular/core';

import { ProduitComponent } from '../produit/produit.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServiceService } from '../service.service';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  data: any;
  constructor( 
    @Inject(MAT_DIALOG_DATA) data: any,
  private serviceService: ServiceService,
  private produitComponent: ProduitComponent,
  private dialogRef: MatDialogRef<DialogComponent>
  
) {
  this.data = data.data;
}


  ngOnInit(): void {
 
  console.log('data', this.data);
  
}
closeDialog() {
  this.dialogRef.close();
}
 
}
