import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ServiceService } from '../service.service';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { ProduitComponent } from '../produit/produit.component';
import { Dialog } from '@angular/cdk/dialog';
import { BackComponent } from '../back/back.component';


@Component({
  selector: 'app-dialog-update',
  templateUrl: './dialog-update.component.html',
  styleUrls: ['./dialog-update.component.css'],
})
export class DialogUpdateComponent implements OnInit {
  data: any;
  usersForm!: FormGroup;
  produit: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) data: any,
    private serviceService: ServiceService,
    private formBuilder: FormBuilder,
    private backComponent: BackComponent,
    private dialogRef: MatDialogRef<DialogUpdateComponent>
    
  ) {
    this.data = data.data;
  }
  completForm() {
    this.usersForm = this.formBuilder.group({
      nom_produit: [this.data.nom_produit, Validators.required],
      categorie_produit: [this.data.categorie_produit, Validators.required],
      description: [this.data.description, Validators.required],
      photo_produit: [this.data.photo_produit, Validators.required],
      prix_produit: [this.data.prix_produit, Validators.required],
    });
  }
  ngOnInit(): void {
    console.log('data', this.data);
    this.completForm();
  }
  closeDialog() {
    this.dialogRef.close();
  }

  
  update(): void {
    console.log('id dans component', this.data.id_produit)
    this.serviceService.UpdateOneProduit(this.data.id_produit, this.usersForm.value).subscribe((data: any) => {
      this.closeDialog();
      
      this.backComponent.getAllProduits()
    });
  }
}
